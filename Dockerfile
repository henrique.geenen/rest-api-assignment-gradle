FROM openjdk:13-jdk-alpine
ADD ./build/libs/restapi-0.0.1-SNAPSHOT*.jar app.jar
EXPOSE 8080
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "app.jar"]