package com.example.restapi.car.application;

import com.example.restapi.car.model.Car;
import com.example.restapi.car.domain.CarService;

import lombok.extern.slf4j.Slf4j;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping(path = "/api/car")
public class CarController {

    private final CarService carService;

    @Autowired
    public CarController(CarService carService) { this.carService = carService; }

    @GetMapping
    public List<Car> getCars() {
        log.info("############### Get endpoint called ###############");
        return carService.getCars();
    }

    @GetMapping(path = "{carId}")
    public Optional<Car> getCarById(@PathVariable("carId") Long carId) {
        log.info("############### GetById endpoint called ###############");
        return carService.getCarById(carId);
    }

    @PostMapping
    public String registerNewCar(@RequestBody Car car) {
        log.info("############### Post endpoint called ###############");
        return carService.addNewCar(car) + " posted";
    }

    @DeleteMapping(path = "{carId}")
    public String deleteCar(@PathVariable("carId") Long carId)  {
        log.info("############### Delete endpoint called ###############");
        return "Car with id " + carService.deleteCar(carId) + " deleted";
    }

    @PutMapping(path = "{carId}")
    public String updateCar(@PathVariable("carId") Long carId,
                            @RequestParam(required = false) String brand,
                            @RequestParam(required = false) String model,
                            @RequestParam(required = false) Integer year) {
        log.info("############### Update endpoint called ###############");
        return "Car with id " + carId + " updated to " + carService.updateCar(carId, brand, model, year);
    }
}
