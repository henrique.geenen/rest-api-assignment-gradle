package com.example.restapi.car.domain;

import com.example.restapi.car.infrastructure.CarRepository;
import com.example.restapi.car.model.Car;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class CarService {

    private final CarRepository carRepository;

    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getCars() {
        log.info("############### Getting cars... ###############");
        return carRepository.findAll();
    }

    public Optional<Car> getCarById(Long carId) {

        boolean exists = carRepository.existsById(carId);
        if (!exists) {
            throw new IllegalStateException("car with id " + carId + " does not exists");
        }
        log.info("############### Getting car with id " + carId + "... ###############");
        return carRepository.findById(carId);
    }

    public Car addNewCar(Car car) {

        Optional<Car> carOptional = carRepository
                .findCarByModel(car.getModel());
        if (carOptional.isPresent()) {
            throw new IllegalStateException("car with model " + car.getModel() + " already exists");
        }
        if (car.getModel().isBlank() || car.getBrand().isBlank() || car.getYear() == null) {
            throw new IllegalStateException("car must contain Model, Brand and Year parameters not null");
        } else {
            log.info("############### Adding " + car + "... ###############");
            carRepository.save(car);
            return car;
        }
    }

    public Long deleteCar(Long carId) {

        boolean exists = carRepository.existsById(carId);
        if(!exists) {
            throw new IllegalStateException("car with id " + carId + " does not exists");
        }
        log.info("############### Deleting car with id " + carId + "... ###############");
        carRepository.deleteById(carId);
        return carId;
    }

    @Transactional
    public Car updateCar(Long carId, String brand, String model, Integer year) {

        Car car = carRepository.findById(carId).orElseThrow(() -> new IllegalStateException("car with id " + carId + " does not exists"));

        log.info("############### Updating car with id " + carId + " to " + car + "... ###############");

        if(brand != null && brand.length() > 0) {
            log.debug("############### Setting up car Brand... ###############");
            car.setBrand(brand);
        }

        if(model != null && model.length() > 0) {
            log.debug("############### Setting up car Model... ###############");
            car.setModel(model);
        }

        if(year != null && year != 0) {
            log.debug("############### Setting up car Year... ###############");
            car.setYear(year);
        }
        return car;
    }
}
