package com.example.restapi.car.application;

import com.example.restapi.car.model.Car;
import com.example.restapi.car.domain.CarService;

import org.json.simple.JSONObject;

import org.junit.jupiter.api.*;

import org.mockito.Mock;
import org.mockito.Mockito;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.web.server.LocalServerPort;

import io.restassured.response.Response;

import javax.annotation.PostConstruct;

import java.util.ArrayList;
import java.util.List;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class CarControllerIntegrationTest {

    @Mock private Car car;
    @Mock private List<Car> cars;

    @PostConstruct
    void setUp() throws Exception {
        car = new Car();
        car.setId(1L);
        car.setBrand("Ferrari");
        car.setModel("488 Pista");
        car.setYear(2020);

        cars = new ArrayList<Car>();
        cars.add(new Car(1L,"McLaren","Speedtail", 2020));
        cars.add(new Car(2L,"Porsche","GT3 RS",2020));

        baseURI = "http://localhost:" + port + "/";
    }

    @LocalServerPort
    private int port;

    @MockBean private CarService carService;

    @Test
    public void anyMethodWhenPathAreIncorrectShouldReturnStatus404() {

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .when()
                .get(baseURI + "api/cars")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(404, response.statusCode());
    }

    @Test
    public void getCarsWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        Mockito.when(carService.getCars()).thenReturn(cars);

        Response response = given()
                .auth()
                .basic("avenue1","admin1")
                .when()
                .get(baseURI + "api/car")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void getCarsWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        Mockito.when(carService.getCars()).thenReturn(cars);

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .when()
                .get(baseURI + "api/car")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(1L, response.jsonPath().getLong("id[0]"));
    }

    @Test
    public void getCarByIdWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        Mockito.when(carService.getCarById(1L)).thenReturn(java.util.Optional.of(car));

        Response response = given()
                .auth()
                .basic("avenue1","admin1")
                .when()
                .get(baseURI + "api/car/" + car.getId())
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void getCarByIdWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        Mockito.when(carService.getCarById(1L)).thenReturn(java.util.Optional.of(car));

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .when()
                .get(baseURI + "api/car/1")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(1L, response.jsonPath().getLong("id"));
    }

    @Test
    public void postCarWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        JSONObject requestParams = new JSONObject();

        requestParams.put("id", car.getId());
        requestParams.put("brand", car.getBrand());
        requestParams.put("model", car.getModel());
        requestParams.put("year", car.getYear());

        Mockito.when(carService.addNewCar(car)).thenReturn(car);

        Response response = given()
                .auth()
                .basic("avenue1","admin1")
                .header("Content-Type","application/json")
                .body(requestParams.toJSONString())
                .when()
                .post("api/car/")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
        Assertions.assertEquals("", response.getBody().asString());
    }

    @Test
    public void postCarWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        JSONObject requestParams = new JSONObject();

        requestParams.put("id", car.getId());
        requestParams.put("brand", car.getBrand());
        requestParams.put("model", car.getModel());
        requestParams.put("year", car.getYear());

        Mockito.when(carService.addNewCar(car)).thenReturn(car);

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .header("Content-Type","application/json")
                .body(requestParams.toJSONString())
                .when()
                .post("api/car/")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals(car + " posted", response.getBody().asString());
    }

    @Test
    public void deleteCarWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        Mockito.when(carService.deleteCar(car.getId())).thenReturn(car.getId());

        Response response = given()
                .auth()
                .basic("avenue1","admin1")
                .when()
                .delete("api/car/1")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void deleteCarWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        Mockito.when(carService.deleteCar(car.getId())).thenReturn(car.getId());

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .when()
                .delete("api/car/" + car.getId())
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("Car with id " + car.getId() + " deleted", response.getBody().asString());
    }

    @Test
    public void putCarWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        Mockito.when(carService.updateCar(car.getId(), car.getBrand(), car.getModel(), car.getYear())).thenReturn(car);

        Response response = given()
                .auth()
                .basic("avenue1","admin1")
                .when()
                .put("api/car/" + car.getId() + "?brand=" + car.getBrand() + "?model=" + car.getModel() + "?year=" + car.getYear())
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
        Assertions.assertEquals("", response.getBody().asString());
    }

    @Test
    public void putCarWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        String requestURI = ("api/car/" + car.getId() + "?brand=" + car.getBrand() + "&model=" + car.getModel() + "&year=" + car.getYear()).replace(" ", "%20");

        Mockito.when(carService.updateCar(car.getId(), car.getBrand().replace(" ", "%20"), car.getModel().replace(" ", "%20"), car.getYear())).thenReturn(car);

        Response response = given()
                .auth()
                .basic("avenue","admin")
                .when()
                .put(requestURI)
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
        Assertions.assertEquals("Car with id " + car.getId() + " updated to " + car, response.getBody().asString());
    }
}
