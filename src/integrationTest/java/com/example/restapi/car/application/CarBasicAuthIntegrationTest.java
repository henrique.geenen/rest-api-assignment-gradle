package com.example.restapi.car.application;

import io.restassured.response.Response;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import javax.annotation.PostConstruct;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.ANY)
public class CarBasicAuthIntegrationTest {

    @PostConstruct
    void setUp() throws Exception {
        baseURI = "http://localhost:" + port + "/";
    }

    @LocalServerPort
    private int port;

    @Test
    public void basicAuthWhenUsernameAndPasswordAreCorrectShouldReturnStatus200() {

        Response response = given()
                .port(port)
                .auth()
                .basic("avenue","admin")
                .when()
                .get(baseURI + "api/car")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(200, response.statusCode());
    }

    @Test
    public void basicAuthWhenUsernameAndPasswordAreIncorrectShouldReturnStatus401() {

        Response response = given()
                .port(port)
                .auth()
                .basic("avenue1","admin1")
                .when()
                .get(baseURI + "api/car")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(401, response.statusCode());
    }

    @Test
    public void basicAuthWhenPathAreIncorrectShouldReturnStatus404() {

        Response response = given()
                .port(port)
                .auth()
                .basic("avenue","admin")
                .when()
                .get(baseURI + "api/cars")
                .then()
                .extract()
                .response();

        Assertions.assertEquals(404, response.statusCode());
    }
}
