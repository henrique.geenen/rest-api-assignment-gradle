package com.example.restapi.car.infrastructure;

import com.example.restapi.car.model.Car;

import org.junit.jupiter.api.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import javax.annotation.PostConstruct;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@DataJpaTest
public class CarRepositoryIntegrationTest {

    @Autowired
    private CarRepository carRepository;

    private Car car;
    private List<Car> cars;

    @PostConstruct
    void setUp() throws Exception {
        car = new Car();
        car.setBrand("Ferrari");
        car.setModel("488 Pista");
        car.setYear(2020);

        cars = new ArrayList<Car>();
        cars.add(new Car("McLaren","Speedtail", 2020));
        cars.add(new Car("Porsche","GT3 RS",2020));
    }

    @AfterEach
    void tearDown() {
        carRepository.deleteAll();
    }

    @Test
    public void findCarByModelShouldReturnCar() {
        //Given
        carRepository.save(car);

        //When
        Optional<Car> expected = carRepository.findCarByModel(car.getModel());

        //Then
        Assertions.assertEquals(Optional.of(car), expected);
    }
}
