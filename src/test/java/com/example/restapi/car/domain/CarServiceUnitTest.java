package com.example.restapi.car.domain;

import com.example.restapi.car.model.Car;
import com.example.restapi.car.infrastructure.CarRepository;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CarServiceUnitTest {

    @Mock private CarRepository carRepository;
    private CarService carService;

    @BeforeEach
    void setUp() { carService = new CarService(carRepository); }

    @Test
    public void canGetCars() {
        //When
        carService.getCars();

        //Then
        verify(carRepository).findAll();
    }

    @Test
    public void canGetCarById() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);
        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));
        when(carRepository.existsById(car.getId())).thenReturn(true);

        //When
        carService.getCarById(car.getId());

        //Then
        verify(carRepository).findById(car.getId());
    }

    @Test
    public void canAddNewCar() {
        //Given
        Car car = new Car("Ferrari","488 Pista",2020);

        //When
        carService.addNewCar(car);

        //Then
        verify(carRepository).save(car);
    }

    @Test
    public void canDeleteCar() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);
        when(carRepository.existsById(car.getId())).thenReturn(true);

        //When
        carService.deleteCar(car.getId());

        //Then
        verify(carRepository).deleteById(car.getId());
    }

    @Test
    public void canUpdateCar() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);
        String brand = "Ferrari";
        String model = "Portofino";
        Long id = 1L;
        Integer year = 2020;
        when(carRepository.findById(car.getId())).thenReturn(Optional.of(car));

        //When
        carService.updateCar(id, brand, model, year);

        //Then
        assertThat(car.getId()).isEqualTo(id);
        assertThat(car.getBrand()).isEqualTo(brand);
        assertThat(car.getModel()).isEqualTo(model);
        assertThat(car.getYear()).isEqualTo(year);
    }

    @Test
    public void willThrowWhenGetCarByIdAndIdNotFound() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);

        //When
        when(carRepository.existsById(car.getId())).thenReturn(false);

        //Then
        assertThatThrownBy(() -> carService.getCarById(car.getId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("car with id " + car.getId() + " does not exists");
    }

    @Test
    public void willThrowWhenDeleteCarAndIdNotFound() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);

        //When
        when(carRepository.existsById(car.getId())).thenReturn(false);

        //Then
        assertThatThrownBy(() -> carService.deleteCar(car.getId()))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("car with id " + car.getId() + " does not exists");
    }

    @Test
    public void willThrowWhenAddNewCarAndModelAlreadyExists() {
        //Given
        Car car = new Car(1L,"Ferrari","488 Pista",2020);

        //When
        when(carRepository.findCarByModel(car.getModel())).thenReturn(Optional.of(car));

        //Then
        assertThatThrownBy(() -> carService.addNewCar(car))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("car with model " + car.getModel() + " already exists");
    }

    @Test
    public void willThrowWhenAddNewCarWithBlankParam() {
        //When
        //Car model is blank
        Car car = new Car("Ferrari","",2020);

        //Then
        assertThatThrownBy(() -> carService.addNewCar(car))
                .isInstanceOf(IllegalStateException.class)
                .hasMessageContaining("car must contain Model, Brand and Year parameters not null");
    }
}
